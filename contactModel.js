var mongoose = require('mongoose');

let contactSchema = mongoose.Schema({
    name: {
        type:String,
        required:true
    },
    email: {
        type:String,
        required:true
    },
    phone: String,
    gender: String,
    create_date:{
        type: Date,
        Default: Date.now
    }
});

var Contact = module.exports = mongoose.model('contacts', contactSchema);

module.exports.get = function(callback, limit) {
    Contact.find(callback).limit(limit);
}