let express = require('express');

let bodyParser = require('body-parser');

let mongoose = require('mongoose');

let app = express();

let apiRoutes = require("./api-routes");

app.use(bodyParser.urlencoded({
    extended:true
}));

app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/nodejsprojects');

var db = mongoose.connection;

if(!db) 
    console.log('Connection not established!');
else 
    console.log('Congratulation! Connection Successful');

    
let port = process.env.PORT || 4700;

app.get('/', (req,res) => res.send('Welcome!'));

app.listen(port, function() {
    console.log('Check if this works at ' + port);
});

app.use('/api', apiRoutes);