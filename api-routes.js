let router = require('express').Router();

router.get('/', function(req,res) {
    res.json({
        status: 'Message Received',
        message: 'Api Routing'
    });
});

//Import contactController

var contactController = require('./contactController');

router.route('/contacts').get(contactController.index);

router.route('/contacts').post(contactController.new);

module.exports = router;