var Contact = require('./contactModel');

exports.index = function (req,res) {
    Contact.get(function(err,contacts){
        if(err) {
            res.json({
                status: 'Error',
                message: err
            })
        }
        res.json({
            status: 'Congratulations',
            message: 'List the contacts',
            data: contacts
        });
    });
};

exports.new = function(req,res) {
    var contact = new Contact();
    contact.name = req.body.name;
    contact.gender = req.body.gender;
    contact.email = req.body.email;
    contact.phone = req.body.phone;
    contact.save(function(err) {
        if(err) {
            res.json(err);
        }
        res.json({
            message: 'New Contact Created',
            data: contact
        });
    });
};

exports.view = function(req,res){
    Contact.findById(req.params.contact_id, function(err,contacts){
        if(err) {
            res.send(err);
        }
        res.json({
            status:'Contacts are loading',
            data:contacts
        });
    });
};


exports.delete = function(req,res){
    Contact.remove(req.params.contact_id, function(err, contacts) {
        if(err) {
            res.send(err);
        }
        res.json({
            status:"suceess",
            message: 'Contact deleted'

        });
    });
};


